# Polywell Simulation

### Authors:
- **Abigail Nicolas Sayago**
- **Sergio Gabriel Sanchez Valencia**

<img src="https://searleser97.gitlab.io/competitive-programming-notes/Util/worms.gif"/>

## Description

This animation is a variation of the _Polywell Simulation_ with the following features:

- Straight lines
- Circular Rotations
- ZigZag with different radius variations

## Computer Graphics fundamentals
Graphics that we can see in our computer, mobile or tablet are produced by using various techniques and algorithms. Computer graphics studies the science and art of communicate visually via computer display.

As a multidisciplinary field, computer graphics makes use of physics, mathematics, engineering and graphic desing. Physics is commonly used to model and perform simulations for animation, mathematics to describe shapes, engineering to optimize memory and processor time.


## Straight lines

Drawing straight lines in a computer is not as straight forward as it looks like, this is mainly because we have a finite amount of pixels that we can paint in the screen, therefore several algorithms have been developed to improve the accuracy of the lines. In our case we decided to us DDA algorithm to draw lines more accurately since the implementation of this algorithm adapts pretty well with our main algorithm.

### DDA Algorithm

Given coordinates:
* Starting = $`(x_{0}, y_{0})`$
* Ending = $`(x_{n}, y_{n})`$

Following steps:

1. Calculate $`\Delta x`$, $`\Delta y`$ and $`m`$ fron given input.
    * $`\Delta x = x_{n} - x_{0}`$
    * $`\Delta y = y_{n} - y_{0}`$
    * $`m = \frac{\Delta y}{\Delta x}`$

2. Find number of points between starting and ending coordinates.
    * if $`( |\Delta x| > |\Delta y| )`$ then  $`steps = |\Delta x|`$
    * else $`steps = |\Delta y|`$

3. If current point is $`(x_{p}, y_{p})`$ and next point is $`(x_{p+1}, y_{p+1})`$
    * if $`m < 1`$ then:
        * $`x_{p+1} = round(1 + x_{p})`$
        * $`y_{p+1} = round (m + y_{p})`$
    * if $`m == 1`$ then:
        * $`x_{p+1} = round (1 + x_{p})`$
        * $`y_{p+1} = round (1 + y_{p})`$
    * if $`m > 1`$ then:
        * $`x_{p+1} = round(\frac{1}{m}+ x_{p})`$
        * $`x_{p+1} = round(1 + y_{p})`$

4. Keep repeating step 3 until end point is reached.
         
Advantages:
* Simple algorithm.
* Easy to implement.
* Avoid using multiplication operation.

Disadvantages:
* Extra overhead of using round function.
* Points generated are not accurate.
* Simple and easy to implement.


## Circles

Circles are mainly used to change our current direction. While we are rotating we have 3 possible outcomes that can happen at any moment of this rotation:

1. Exit smoothly from the rotation and continue with a straight line. (it is possible that we exit in the same point where we started the rotation)
2. Keep rotating in the same direction
3. Start rotating to the opposite side.


To create circular rotations we use the well-known concept of _**Uniform Circular Motion**_

<!--<img src="https://www.physicsclassroom.com/mmedia/circmot/ucm.gif"/>-->
<img src="https://searleser97.gitlab.io/computer-graphics/images/circle.PNG"/>

<p>

</p>

We utilised the following rotation formulas to calculate the position of the next point and then we used point traslation to locate the point in the correct position in the 2D plane since this formulas assume we are located in the origin of the plane.

$`x' = x\cos(\theta) - y\sin(\theta)`$

$`y' = x\sin(\theta) + y\cos(\theta)`$

### Implementation details

Initially we will always have an starting vector which will be pointing in some direction. From this vector we decide randomly whether we want to rotate to the right or to the left, independently of the decision we then compute the size of the radius that we will use for the rotation. With this information we can compute the center of our circle and apply above formulas to get the position of the next point.

## Zig Zag

With circular rotation and straight lines already explained, then it is easy to deduce that to make a zig zag movement we can just combine previous movements, changing the direction ocasionally.


## Drawing points

We used **gfx** library in this animation. This library helps us to paint a pixel in the screen easily. So, we had to focus in drawing lines or circles. For this we had to maintain a set of coordinates that represented our lines or circles in the plane, so we decided to use a _Queue_ to store these coordinates, this allowed us to draw $`K`$ points in each iteration, by poping old points and pushing new points to the _queue_.

To add a new point to the _queue_ we use vector addition between last point in _queue_ and a _unit_ vector that already has the correct direction

<img src="https://searleser97.gitlab.io/computer-graphics/images/lines.PNG"/>

